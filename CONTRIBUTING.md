
Contributing to these examples
------------------------------

Re-running and then committing changes to a notebook will quickly use a lot of space for the git history.
These notebooks are used in the official documentation, so that history must be preserved. To keep the size
of this git repository small, only commit rerun notebooks when absolutely necessary.

.. code-block::

   * Markdown cells can be modified in place without needing to rerun the notebook
   * Any change to code require a re-run to get consistent output - only propose such changes
     when doing so substantially improves a notebook.
